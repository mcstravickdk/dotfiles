{
  perSystem = {
    pkgs,
    lib,
    config,
    self',
    ...
  }: {
    imports = [
      ./dev-shells.nix
      ./mission-control.nix
    ];
  };
}
