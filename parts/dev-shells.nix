{
  pkgs,
  config,
  ...
}: {
  devShells = {
    default = pkgs.mkShell {
      inputsFrom = [config.mission-control.devShell];
      packages = with pkgs; [alejandra];
    };
  };
}
