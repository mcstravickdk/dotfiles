let
  direnv = import ./direnv.nix;
  git = import ./git.nix;
  homeManager = import ./home-manager.nix;
in {
  flake.homeModules = {
    inherit
      direnv
      git
      homeManager
      ;
    commandline = {imports = [direnv git homeManager];};
  };
}
