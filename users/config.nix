let
  myself = "darren";
in {
  inherit myself;
  users = {
    "${myself}" = {
      name = "Darren McStravick";
      email = "darren.mcstravick1@gmail.com";
      sshKeys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKogc+5HBh1GSsEN1vqTchxRh0ERCJ6PmQnr+X7efhdB isaac@desktop"
      ];
    };
  };
}
