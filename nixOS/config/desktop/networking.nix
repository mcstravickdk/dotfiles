{lib, ...}: {
  networking = {
    hostName = "desktop";
    networkmanager.enable = true;
    useDHCP = lib.mkDefault true;
  };
}
