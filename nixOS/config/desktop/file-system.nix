{
  fileSystems."/boot/efi" = {
    device = "/dev/disk/by-uuid/CF91-9759";
    fsType = "vfat";
  };

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/f072408c-4c1a-49ed-8513-6d626451f9d9";
    fsType = "ext4";
  };
}
