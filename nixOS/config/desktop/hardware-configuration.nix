{
  config,
  lib,
  modulesPath,
  ...
}: {
  hardware.cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

  imports = [(modulesPath + "/installer/scan/not-detected.nix")];
}
