let
  environment = import ./environment.nix;
  locale = import ./locale.nix;
  nix = import ./nix.nix;
  printing = import ./printing.nix;
  sound = import ./sound.nix;
  system = import ./system.nix;
  user = import ./user.nix;
  xserver = import ./xserver.nix;
in {
  flake = {
    nixosModules = {
      inherit
        environment
        locale
        nix
        printing
        sound
        system
        user
        xserver
        ;
      base = {imports = [environment nix system user];};
      desktop = {imports = [locale printing sound xserver];};
    };
  };
}
