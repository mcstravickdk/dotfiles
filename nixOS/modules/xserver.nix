{
  services.xserver = {
    displayManager.sddm.enable = true;
    desktopManager.plasma5.enable = true;
    enable = true;
    layout = "us";
  };
}
