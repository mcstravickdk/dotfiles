{
  pkgs,
  flake,
  ...
}: {
  users.users.${flake.config.people.myself} = {
    isNormalUser = true;
    extraGroups = [
      "kvm"
      "networkmanager"
      "wheel"
    ];
    shell = pkgs.bash;
  };
}
