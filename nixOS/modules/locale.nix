{
  time.timeZone = "Australia/Perth";

  i18n = {
    defaultLocale = "en_AU.utf8";

    extraLocaleSettings = {
      LC_ADDRESS = "en_AU.utf8";
      LC_IDENTIFICATION = "en_AU.utf8";
      LC_MEASUREMENT = "en_AU.utf8";
      LC_MONETARY = "en_AU.utf8";
      LC_NAME = "en_AU.utf8";
      LC_NUMERIC = "en_AU.utf8";
      LC_PAPER = "en_AU.utf8";
      LC_TELEPHONE = "en_AU.utf8";
      LC_TIME = "en_AU.utf8";
    };
  };
}
