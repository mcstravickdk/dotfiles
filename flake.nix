{
  description = "Skeleton";

  inputs = {
    flake-root.url = "github:srid/flake-root";
    home-manager.url = "github:nix-community/home-manager";
    mission-control.url = "github:Platonic-Systems/mission-control";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    pre-commit-hooks-nix.url = "github:cachix/pre-commit-hooks.nix";
  };

  outputs = inputs @ {
    flake-parts,
    self,
    ...
  }: let
    stateVersion = "22.11";
    system = "x86_64-linux";
  in
    flake-parts.lib.mkFlake {inherit inputs;} {
      imports = [
        inputs.flake-root.flakeModule
        inputs.mission-control.flakeModule
        inputs.pre-commit-hooks-nix.flakeModule
        ./lib
        ./parts
        ./home-manager/modules
        ./nixOS/modules
        ./users
      ];
      flake = {config, ...}: {
        homeConfigurations = {
          desktop =
            self.lib.mkHome
            [
              ./home-manager
              config.homeModules.commandline
            ]
            stateVersion
            system;
        };
        nixosConfigurations = {
          desktop =
            self.lib.mkLinuxSystem
            [
              ./nixOS/config/desktop
              config.nixosModules.base
              config.nixosModules.desktop
            ]
            stateVersion
            system;
        };
      };
      systems = [system];
    };
}
