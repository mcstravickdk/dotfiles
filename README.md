# NixOS Skeleton

This is a skeleton for a flake-based NixOS configuration that uses Home Manager.

- Change these files to fit your system:
	- `nixOS/config/desktop/boot.nix`
	- `nixOS/config/desktop/file-system.nix`
	- `nixOS/config/desktop/hardware-configuration.nix`
	- `user/config.nix`